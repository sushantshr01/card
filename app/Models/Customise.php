<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customise extends Model
{
    protected $fillable = [
        "type",
        "purpose",
        "size",
        "background_color",
        "text_color",
        "description",
        "name",
        "delievery_address",
        "number",
        "contact_type"
    ];

    const TYPE = [
        "Business card",
        "Envelopes",
        "Posters",
        "Flyers",
        "Greeting card",
        "Occassional card",
    ];

    const SIZE = [
        "3x5",
        "3.5x4.8",
        "3x5",
        "4x9",
        "5.5x7.5",
        "5x7",
        "7x5",
        "14x5"
    ];

    const COLOR = [
        "Red",
        "Blue",
        "Green",
        "Black",
        "Golden",
        "White",
        "Pink",
        "Purple",
    ];

    const CONTACT_TYPE = [
        "Call",
        "Email",
        "Message"
    ];
}
