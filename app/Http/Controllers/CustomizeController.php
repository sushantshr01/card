<?php

namespace App\Http\Controllers;

use App\Models\Customise;
use Illuminate\Http\Request;

class CustomizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.pages.customize');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customize = Customise::create([
            "type" => $request->type,
            "purpose" => $request->purpose,
            "size" => $request->size,
            "background_color" => $request->background_color,
            "text_color" => $request->text_color,
            "description" => $request->description,
            "name" => $request->firstname . " ". $request->lastname,
            "delievery_address" => $request->delievery_address,
            "number" => $request->number,
            "contact_type" => $request->contact_type
        ]);

        return redirect()->route("home");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function backIndex()
    {
        $customizes = Customise::paginate(10);

        return view("backend.customize.index", compact("customizes"));
    }
}
