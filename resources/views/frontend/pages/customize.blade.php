@extends('frontend.layouts.master')

@section('title','Banla Wa Prints || Customize')

@section('main-content')

<div  class="offset-1 col-10 col-md-8">
	<form action="{{ route("customize.store") }}" method="post" autocomplete="on" id="formSubmit">
		<br />
        @csrf
		<div class="form-group row">
    		<label for="product" class=" col-md-3 col-form-label"> Product type </label>
    		<div class="col-md-8">
            	<select class="form-select form-select-sm mb-3" size="1" name="type">
                    @foreach(\App\Models\Customise::TYPE as $type)
            		    <option value="{{ $loop->index }}"> {{$type}} </option>
                    @endforeach
				</select>
       		</div>
    	</div>
    	<div class="form-group row">
    		<label for="occasions" class=" col-md-3 col-form-label"> Purpose/Event </label>
    		<div class="col-md-9">
    			<input name="purpose" type="text" class="form-control" id="occasions" name="occasions" placeholder="wedding/birthday/businesscard/greeting card/....">
    		</div>
    	</div>

    	 <div class="form-group row">
            <label for="size" class=" col-md-3 col-form-label"> Size </label>
        	<div class="col-md-8">
            	<select class="form-select form-select-sm mb-3" size="1" name="size">
                    @foreach(\App\Models\Customise::SIZE as $size)
                        <option value="{{ $loop->index }}"> {{$size}} </option>
                    @endforeach
				</select>
       		</div>
    	</div>

    	 <div class="form-group row">
            <label for="bg" class=" col-md-3 col-form-label"> Background colour  </label>
            <div class="col-md-8">
            	<select class="form-select form-select-sm mb-3" size="1" name="background_color">
                    @foreach(\App\Models\Customise::COLOR as $color)
                        <option value="{{ $loop->index }}"> {{$color}} </option>
                    @endforeach
				</select>
            </div>
    	</div>

		<div class="form-group row">
            <label for="bg" class=" col-md-3 col-form-label">  Colour of Text </label>
            <div class="col-md-8">
            	<select class="form-select form-select-sm mb-3" size="1" name="text_color" >
                    @foreach(\App\Models\Customise::COLOR as $color)
                        <option value="{{ $loop->index }}"> {{$color}} </option>
                    @endforeach
				</select>
            </div>
    	</div>

		<div class="form-group row">
			<label for="requests" class=" col-md-3 col-form-label"> <strong> Additional details for cutsomization </strong> </label>
			<div class="col-md-9">
				<textarea name="description" rows="6" cols="25"></textarea><br />
			</div>
		</div>
		<div class="form-group row">
    		<label for="firstname" class=" col-md-3 col-form-label"> First name </label>
    		<div class="col-md-9">
    			<input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name">
    		</div>
    	</div>
    	<div class="form-group row">
            <label for="lastname" class=" col-md-3 col-form-label"> Last name </label>
            <div class="col-md-9">
            	<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name">
            </div>
    	</div>

    	<div class="form-group row">
    		<label for="place" class=" col-12 col-md-3 col-form-label"> Delivery to </label>
    		<div class="col-5 col-md-4">
    			<input type="text" class="form-control" id="place" name="delievery_address" placeholder=" Place for delivery">
    		</div>
    		<div class="col-4 col-md-4">
                <input type="tel" class="form-control" id="telnum" name="number" placeholder="Tel. number">
             </div>
         </div>



		<div class="form-group row">
			<label class=" col-md-3 col-form-label" for ="approve">
                <strong> Preferred way for contact </strong>
            </label>
             <div class="col-md-8">
            	<select class="form-select form-select-sm mb-7" size="1" name="contact_type">
                    @foreach(\App\Models\Customise::CONTACT_TYPE as $contactType)
                        <option value="{{ $loop->index }}"> {{$contactType}} </option>
                    @endforeach
                </select>
            </div>
        </div>

       <div class="form-group row">
            <div class="offset-md-3 col-md-10">
				<button type="submit" class="btn btn-primary"> Send </button>
				<button type="submit" class="btn btn-danger"> Clear </button>
			</div>
		</div>

	</form>
</div>

@endsection
@section('postScripts')
<script>
$('#formSubmit').on('submit', function(event) {
//    event.preventDefault();
   alert("Please wait we will contact you soon");
});
</script>
@endsection
