<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customises', function (Blueprint $table) {
            $table->id();
            $table->integer('type')->nullable();
            $table->string('purpose')->nullable();
            $table->integer('size')->nullable();
            $table->integer('background_color')->nullable();
            $table->integer('text_color')->nullable();
            $table->string('description')->nullable();
            $table->string('name')->nullable();
            $table->string("delievery_address")->nullable();
            $table->string("number")->nullable();
            $table->integer("contact_type")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customises');
    }
}
