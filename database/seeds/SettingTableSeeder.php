<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=array(
            'description'=>"description .",
            'short_des'=>"short secription .",
            'photo'=>"image.jpg",
            'logo'=>'logo_t.png',
            'address'=>"Kathmandu,Nepal",
            'email'=>"Banla.Wa.Prints@gmail.com",
            'phone'=>"0123456789",
        );
        DB::table('settings')->insert($data);
    }
}
